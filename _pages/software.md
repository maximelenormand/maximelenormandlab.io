---
layout: page
title: Software
permalink: /software/
---

<h1 class="page-title">Software</h1>

<div style="height:10px;">&nbsp;</div>

This page gathers 
[R packages](https://www.r-pkg.org/maint/maxime.lenormand@inrae.fr){:target="_blank"} 
and code that I have developed or contributed to. 
The repositories are divided into two categories: those linked to articles 
and/or visualizations, and those that are independent projects. All are available 
on my 
[GitHub](https://github.com/maximelenormand?tab=repositories){:target="_blank"} 
and 
[GitLab](https://gitlab.com/users/maximelenormand/projects){:target="_blank"} 
accounts.

<div style="height:2px;">&nbsp;</div>

<!-- Packages -->
<a id="packages"></a>
<h3 class="page-title">Packages</h3>

<div style="height:10px;">&nbsp;</div>

<a id="bioregionpackage"></a> 
[**bioregion**](https://biorgeo.github.io/bioregion/){:target="_blank"} 
is an R package develop to analyze and compare several methods of 
bioregionalisations.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#bioregionpaper">paper</a>]
<a href="https://hal.inrae.fr/hal-04973583" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<a id="tdlmpackage"></a> 
[**TDLM**](https://epivec.github.io/TDLM/index.html){:target="_blank"} is an R 
package develop to perform systematic comparison of trip distribution laws and 
models.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#tdlmpaper">paper</a>]
<a href="https://hal.science/hal-04973554" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:2px;">&nbsp;</div>

<!-- Repositories -->
<a id="repos">
<h3 class="page-title">Repositories</h3>

<div style="height:10px;">&nbsp;</div>

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">Repositories linked to articles and/or visualizations</span> </h5>

<div style="height:10px;">&nbsp;</div>

<a id="pvsscode">
</a>[**Plant-spectral-diversity**](https://github.com/maximelenormand/Plant-spectral-diversity){:target="_blank"} 
proposes a method to compare and to combine in situ and remote sensing data to 
measure the diversity of plant and spectral species in France.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#pvsspaper">paper</a>]
<a href="https://hal.inrae.fr/hal-04973595" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="bibliocode"></a>
[**Biblio-TETIS**](https://github.com/maximelenormand/biblio-tetis){:target="_blank"} 
contains all the material needed to extract the co-publication network of UMR 
TETIS and run the associated interactive web application.  
**Open science:**
[<a href="https://www.maximelenormand.com/visualizations#biblioviz">viz</a>]
<a href="https://hal.science/hal-04967144" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="msdcode"></a>[**Mobile-service-diversity**](https://github.com/maximelenormand/mobile-service-diversity){:target="_blank"} 
proposes a method to map the diversity of mobile service usage and its relation 
with land use in cities.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#msdpaper">paper</a>]
<a href="https://hal.science/hal-04967182" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="intersectionalitycode"></a>
[**Intersectionality**](https://github.com/maximelenormand/intersectionality){:target="_blank"} 
proposes several tools and metrics to explore mismatch in hourly population 
profiles of French districts for gender, age and educational groups from an 
intersectional point of view.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#intersectionalitypaper">paper</a>]
[<a href="https://www.maximelenormand.com/visualizations#intersectionalityviz">viz</a>]
<a href="https://hal.science/hal-04974786" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="samplebiasdmscode"></a>
[**Sample-bias-correction-SDMs**](https://github.com/maximelenormand/sample-bias-correction-sdms){:target="_blank"} 
proposes a method to assess the effect of sample bias correction in 
Species Distribution Models.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#samplebiasdmspaper">paper</a>]
<a href="https://hal.science/hal-04969364" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="mismatchconnectivitycode"></a>
[**Mismatch-connectivity**](https://github.com/maximelenormand/mismatch-connectivity){:target="_blank"} 
proposes a method to reclassify and compare connectivity maps.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#mismatchconnectivitypaper">paper</a>]
<a href="https://hal.science/hal-04969214" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="gismcdaowacode"></a>
[**GIS-MCDA-OWA**](https://github.com/maximelenormand/gis-mcda-owa){:target="_blank"} 
proposes a methodology based on an efficient exploration of the decision-strategy 
space defined by the level of risk and trade-off in GIS-MCDA with OWA.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#gismcdaowapaper">paper</a>]
<a href="https://hal.science/hal-04969124" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="mppcode"></a>
[**Map-potential-paths**](https://github.com/maximelenormand/map-potential-paths){:target="_blank"} 
proposes a method to map potential paths for livestock movements by combining 
information contained in livestock  mobility networks with landscape connectivity 
based on land use and land cover features.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#mpppaper">paper</a>]
<a href="https://hal.science/hal-04969070" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="biogeomedcode"></a>
[**Biogeographical-network-analysis**](https://github.com/maximelenormand/Biogeographical-network-analysis){:target="_blank"} 
provides an R script to identify and characterize the 
biogeographical structure of a region. It also contains all the materials needed
to run an interactive web application developed to visualize the obtained 
bioregions at different scales.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#biogeomedpaper">paper</a>]
[<a href="https://www.maximelenormand.com/visualizations#biogeomedviz">viz</a>]
<a href="https://hal.science/hal-04956296" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="ahiacode"></a>[**AHIA**](https://github.com/maximelenormand/AHIA){:target="_blank"} 
contains all the materials needed to run an interactive web application to 
visualize socio-ecological interactions at different scales across 16 case 
studies in Europe.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#ahiapaper">paper</a>]
[<a href="https://www.maximelenormand.com/visualizations#ahiaviz">viz</a>]
<a href="https://hal.science/hal-04934619" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>


<div style="height:10px;">&nbsp;</div>

<a id="owacode"></a>
[**OWA-weights-generator**](https://github.com/maximelenormand/OWA-weights-generator){:target="_blank"} 
implements a method to automatically determine ordered weighted averaging (OWA)
weights using truncated distributions. It also contains all the materials needed
to run an interactive web application developed to visualize the obtained 
distribution, to download the associated weights and to design experimental 
deigns.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#owapaper">paper</a>]
[<a href="https://www.maximelenormand.com/visualizations#owaviz">viz</a>]
<a href="https://hal.science/hal-04956268" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="mflcode"></a>
[**Most-frequented-locations**](https://github.com/maximelenormand/Most-frequented-locations){:target="_blank"} 
provides a Python script to extract Most Frequented Locations from 
individual spatio-temporal trajectories.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#mflpaper">paper</a>]
<a href="https://hal.science/hal-04967571" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="tripcode"></a>
[**Trip-distribution-laws-and-models**](https://github.com/maximelenormand/Trip-distribution-laws-and-models){:target="_blank"} 
provides several Java scripts to estimate mobility flows with different types of trip 
distribution laws and models.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#trippaper">paper</a>]
<a href="https://hal.science/hal-04967531" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="apmccode"></a>
[**APMC**](https://github.com/maximelenormand/apmc){:target="_blank"} proposes 
an implementation in R of several Approximate Bayesian Computation (ABC) 
algorithms.  
**Open science:**
[<a href="https://www.maximelenormand.com/publications#apmcpaper">paper</a>]
<a href="https://hal.science/hal-04967106" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">Other repositories</span> </h5>

<div style="height:10px;">&nbsp;</div>

<a id="coauthorshipcode"></a> 
[**Coauthorship-network**](https://github.com/maximelenormand/coauthorship-network){:target="_blank"} 
provides R scripts to create and visualize a coauthorship network with R.  
[<a href="https://www.maximelenormand.com/networks#coauthorship">example</a>]
<a href="https://hal.science/hal-04966986" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

[**Inkscape-presentation**](https://github.com/maximelenormand/inkscape-presentation){:target="_blank"} 
provides a Python script that generates a PDF presentation based on the layer(s)
contained in an Inkscape SVG file.
<a href="https://hal.science/hal-04966978" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

[**XS**](https://github.com/maximelenormand/xs){:target="_blank"} 
provides several R scripts to generate a set of Origin-Destination (OD) matrices
based on individual mobility flow information.
<a href="https://hal.science/hal-04966971" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft2"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

[**Flickr**](https://github.com/maximelenormand/Flickr){:target="_blank"} 
provides scripts to automate Flickr queries using the Flickr API
and to automatically send messages to a list of Flickr contacts through a contact
form from your Flickr account.
<a href="https://hal.science/hal-04966936" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft2"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

[**Vessel-trip-from-path**](https://github.com/maximelenormand/Vessel-trip-from-path){:target="_blank"} 
provides two Python scripts to extract and aggregate trips from spatio-temporal 
individual trajectories.
<a href="https://hal.science/hal-04966908" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft2"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<a id="kmlcode"></a>
[**KML-movie-Python**](https://github.com/maximelenormand/KML-movie-Python){:target="_blank"} 
provides a Python script for plotting trajectories over time on Google Earth.  
[<a href="https://www.maximelenormand.com/visualizations#kmlviz">example</a>]
<a href="https://hal.science/hal-04966897" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-soft"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:10px;">&nbsp;</div>

<!-- Websites -->
<h3 class="page-title">Websites</h3>

<div style="height:10px;">&nbsp;</div>

[**My personal academic website**](https://gitlab.com/maximelenormand/maximelenormand.gitlab.io){:target="_blank"} 
made with 
[Jekyll](https://jekyllrb.com/){:target="_blank"} 
based on the 
[Hyde](http://hyde.getpoole.com/){:target="_blank"} theme.

<div style="height:5px;">&nbsp;</div>

[**Website of the NetCost project**](https://gitlab.com/netcost/netcost.gitlab.io){:target="_blank"}
made with 
[Jekyll](https://jekyllrb.com/){:target="_blank"} 
based on the 
[Jackal](https://github.com/clenemt/jackal){:target="_blank"} theme.

<div style="height: 300px;"></div>