---
layout: page
title: Viz
permalink: /Viz/
---

<h1 class="page-title">Data visualizations</h1>

<div style="height:15px;">&nbsp;</div>

<h3 class="page-title">Human diffusion</h3> 

<div style="height:10px;">&nbsp;</div>

<video controls width="1000px">
  <source src="{{ '/assets/vizs/Diffusion_Barcelona.mp4' | relative_url }}" type="video/mp4" />
</video>

<div style="height:50px;">&nbsp;</div>

<h3 class="page-title">Hotspots</h3>

<div style="height:10px;">&nbsp;</div>

<video controls width="600px">
  <source src="{{ '/assets/vizs/Hotspot_Barcelona.mp4' | relative_url }}" type="video/mp4" />
</video>

<div style="height:50px;">&nbsp;</div>

<h3 class="page-title">Credit card trajectories</h3>

<div style="height:10px;">&nbsp;</div>

<video controls width="700px">
  <source src="{{ '/assets/vizs/Trajectories_Madrid.mp4' | relative_url }}" type="video/mp4" />
</video>
