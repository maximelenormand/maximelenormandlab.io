---
layout: page
title: Resume
permalink: /resume/
---

<h1 class="page-title">Resume</h1>

<div style="height:10px;">&nbsp;</div>

On this page, you'll find a brief overview of my academic and professional 
background, with links to an up-to-date
[PDF version of my CV](https://www.mmmycloud.com/index.php/s/CdfiLCGyGSGc2RX){:target="_blank"} 
and 
[my CV on HAL](https://cv.hal.science/maximelenormand){:target="_blank"}.

<div style="height:10px;">&nbsp;</div>

<h3 class="page-title">Work experiences</h3>
 
<span style="color:gray;font-weight: bold;">Since 2016</span> &nbsp; **Research Associate** &nbsp; [TETIS (INRAE), Montpellier, France](https://www.umr-tetis.fr/index.php/fr/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2013-2016</span> <span style="display:inline-block; width:0.07cm;"></span> &nbsp; **Postdoctoral Research Associate** &nbsp; [IFISC (UIB-CSIC), Palma de Mallorca, Spain](http://ifisc.uib-csic.es/){:target="_blank"}   
<span style="color:gray;font-weight: bold;">2010-2012</span> <span style="display:inline-block; width:0.07cm;"></span> &nbsp; **PhD student** &nbsp; [LISC (INRAE), Clermont-Ferrand, France](https://lisc.inrae.fr/){:target="_blank"}  

<div style="height:20px;">&nbsp;</div>

<h3 class="page-title">Academic experiences</h3>

<!---
<span style="color:gray;font-weight: bold;">2025</span> &nbsp; **HDR[*](https://en.wikipedia.org/wiki/Habilitation){:target="_blank"}  in Computer Science** &nbsp; [University of Montpellier, Montpellier, France](https://www.umontpellier.fr/en/){:target="_blank"}  
-->
<span style="color:gray;font-weight: bold;">2012</span> &nbsp; **PhD in Computer Science** &nbsp; [Blaise Pascal University, Clermont-Ferrand, France](https://www.uca.fr/en/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2009</span> &nbsp; **MSc in Statistics and Data Analysis** &nbsp; [Blaise Pascal University, Clermont-Ferrand, France](https://www.uca.fr/en/){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2007</span> &nbsp; **BSc in Mathematics** &nbsp; [University of Rennes 1, Rennes, France](https://www.univ-rennes1.fr/en){:target="_blank"}  

<div style="height:20px;">&nbsp;</div>

<h3 class="page-title">Grants & scholarships</h3>

<span style="color:gray;font-weight: bold;">2018</span> &nbsp; **ANR JCJC NetCost (4 years - 160K&#8364;)** &nbsp; [French National Research Agency](https://anr.fr/en/#){:target="_blank"}  
<span style="color:gray;font-weight: bold;">2014</span> &nbsp; **Postdoctoral grant (2 years)** &nbsp; [Govern de les Illes Baleares](http://www.caib.es/govern/index.do?lang=ca){:target="_blank"}   
<span style="color:gray;font-weight: bold;">2012</span> &nbsp; **Travel award (500$)** &nbsp; [International Society for Bayesian Analysis](https://bayesian.org/){:target="_blank"}   
<span style="color:gray;font-weight: bold;">2010</span> &nbsp; **PhD grant (3 years)** &nbsp; [Irstea & Regional council of Auvergne](http://www.auvergne.fr/){:target="_blank"}   
<span style="color:gray;font-weight: bold;">2006</span> &nbsp; **CREPUQ international exchange grant (1 year)** 
