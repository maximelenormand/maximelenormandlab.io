---
layout: page
title: Talks
permalink: /talks/
---

<h1 class="page-title">Talks</h1>

<div style="height:15px;"></div>

This page gathers my talks, organized by year, 
with links to the corresponding slides.

<div style="height:10px;"></div>

<h3 class="page-title">2024</h3>
* [Mobile service usage diversity and land use organisation in cities](https://www.mmmycloud.com/index.php/s/85QEfwQxFfNnXbX){:target="_blank"} 
[*Café-MTD*]
* [Spatial networks, data & complexity](https://www.mmmycloud.com/index.php/s/cjzRMYJ9t9dZajK){:target="_blank"} 
[*MISTEA*]

<h3 class="page-title">2023</h3>
* [Species Distribution Model using Remote-Sensed Dynamic Habitat Index](https://www.mmmycloud.com/index.php/s/5CBPJ68QwJG29F2){:target="_blank"} 
[*IUFRO*]
<a href="https://hal.inrae.fr/hal-04256967" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Mapping mobile service usage diversity in cities](https://www.mmmycloud.com/index.php/s/KePbjY3xGdWeEkK){:target="_blank"}
[*NetMob*]
<a href="https://hal.inrae.fr/hal-04231514" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2022</h3>
* [bioRgeo: Bioregionalisation Methods in R](https://www.mmmycloud.com/index.php/s/gHgbjrn6WqD2Ndr){:target="_blank"} 
[*CCS*]
<a href="https://hal.inrae.fr/hal-03817581" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Major Tom to Ground Control](https://www.mmmycloud.com/index.php/s/foD2T54besTAmoG){:target="_blank"} 
[*Café-MTD*]

<h3 class="page-title">2021</h3>
* [Intersectional approach of everyday geography](https://www.mmmycloud.com/index.php/s/GHmPGbZmSN3Xcwm){:target="_blank"} 
[*UrbanSys*]
<a href="https://hal.inrae.fr/hal-03406971" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Intersectional approach of everyday geography](https://www.mmmycloud.com/index.php/s/GHmPGbZmSN3Xcwm){:target="_blank"} 
[*TETIS*]
* [Toutes choses égales par ailleurs](https://www.mmmycloud.com/index.php/s/9xKmgjAdPFZaWan){:target="_blank"} 
[*TETIS*]

<h3 class="page-title">2020</h3>
* [Hierarchical socioeconomic structure of spatial and social interactions](https://www.mmmycloud.com/index.php/s/2fZkn5jyKmPtP6q){:target="_blank"} 
[*TETIS*]
* [Hierarchical socioeconomic structure of spatial and social interactions](https://www.mmmycloud.com/index.php/s/ixGRAm6GM9yWHCJ){:target="_blank"} 
[*Sociophysics*]

<h3 class="page-title">2019</h3>
* [Ecosystem services, land-use planning and Multi-Criteria Decision Analysis](https://www.mmmycloud.com/index.php/s/RnpagmiwRkMGybS){:target="_blank"} 
[*ES LAMSADE*]
* [From individual spatio-temporal trajectories to spatial networks](https://www.mmmycloud.com/index.php/s/WrGzcQsyaFSYHTk){:target="_blank"} 
[*XTerM*]
<a href="https://hal.inrae.fr/hal-02890716" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Mesurer l'excess commuting &#224; diff&#233;rentes &#233;chelles](https://www.mmmycloud.com/index.php/s/AzxjQA55YBomPa8){:target="_blank"} 
[*TheoQuant*]
<a href="https://hal.inrae.fr/hal-02889649" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2018</h3>
* [ICT Data, Spatial Networks & Society](https://www.mmmycloud.com/index.php/s/Q4kWqZ3amxY6XWp){:target="_blank"}
[*UFRJ*]
* [Multiscale socio-ecological networks in the age of information](https://www.mmmycloud.com/index.php/s/CpAariosFiCNnS6){:target="_blank"}
[*IUFRO*]
<a href="https://hal.inrae.fr/hal-02608951" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Biogeographical network analysis of plant species distribution](https://www.mmmycloud.com/index.php/s/Qfb9cPoe2AosBSA){:target="_blank"}
[*NetSci*]
<a href="https://hal.inrae.fr/hal-02889646" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Multiscale socio-ecological networks in the age of information](https://www.mmmycloud.com/index.php/s/Kd9gRDn2wZm5M4n){:target="_blank"}
[*NetSci*]
<a href="https://hal.inrae.fr/hal-02890710" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Data & Network Sciences](https://www.mmmycloud.com/index.php/s/5KkHtknk3QysrmH){:target="_blank"}
[*Ecoinformatica*]
* [Toutes choses égales par ailleurs](https://www.mmmycloud.com/index.php/s/2ZCq9cCnnFnEHm2){:target="_blank"}
[*Carthag&#233;o & G&#233;oprisme*]
* [The NetCost project](https://www.mmmycloud.com/index.php/s/ckSFpd3Edpg3cHJ){:target="_blank"}
[*ANR*]
* [A network approach to assess the cost of change](https://www.mmmycloud.com/index.php/s/q9FSHMqWSA854y6){:target="_blank"}
[*LISC*]

<h3 class="page-title">2017</h3>
* [From territories to complex systems](https://www.mmmycloud.com/index.php/s/PsXBnkk3fBfWAsx){:target="_blank"}
[*GoPro*]
* [Biogeographical network analysis of plant species distribution](https://www.mmmycloud.com/index.php/s/dsdDeTHypL3Q532){:target="_blank"} 
[*CCS*]
<a href="https://hal.inrae.fr/hal-02890693" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Disentangling socio-ecological interactions using geo-tagged photographs](https://www.mmmycloud.com/index.php/s/3rn7GzemGN5Y3es){:target="_blank"} 
[*CCS*]
<a href="https://hal.inrae.fr/hal-02890702" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Spatial networks in ecology](https://www.mmmycloud.com/index.php/s/mLX42ZF4YjyWzSR){:target="_blank"} 
[*IFISC*]
* [Cultural ecosystem services](https://www.mmmycloud.com/index.php/s/3fo4iTHyDzHtAzG){:target="_blank"} 
[*Alter-Net*]
<a href="https://hal.inrae.fr/hal-02606273" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2016</h3>
* [Apport des données TIC dans l'étude des CES](https://www.mmmycloud.com/index.php/s/MNBtrx6HGkH6gck){:target="_blank"} 
[*Ecolo'Tech*]
* [Extraction de flux à partir de trajectoires individuelles](https://www.mmmycloud.com/index.php/s/ajzTo2nxJi7scy7){:target="_blank"} 
[*Journée SIG*]
* [Spatial uncertainty propagation in ICT data analysis](https://www.mmmycloud.com/index.php/s/boo3Hm6RN2qwNRM){:target="_blank"} 
[*UrbanNet*]
<a href="https://hal.inrae.fr/hal-02605894" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Modeling individual human mobility patterns by travel purpose](https://www.mmmycloud.com/index.php/s/yNEAFJ6ywKTfEw2){:target="_blank"} 
[*CSS*]
<a href="https://hal.inrae.fr/hal-02604647" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Extraction de flux à partir de trajectoires individuelles](https://www.mmmycloud.com/index.php/s/kHHTcr7cPTHKDzx){:target="_blank"} 
[*Mobilité CIRAD*]
* [Generating a synthetic population](https://www.mmmycloud.com/index.php/s/akgowtxtbaPQr3x){:target="_blank"} 
[*MISS-ABMS*]
* [Crowdsourcing the Robin Hood effect in the city](https://www.mmmycloud.com/index.php/s/WMMb47k9RzABTiH){:target="_blank"} 
[*Complex Networks*]
<a href="https://hal.inrae.fr/hal-02603639" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Is spatial information in ICT data reliable?](https://www.mmmycloud.com/index.php/s/AiTdjogq69dtAGW){:target="_blank"} 
[*Spatial Accuracy*]
<a href="https://hal.science/hal-01368922" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Human mobility and city attractiveness](https://www.mmmycloud.com/index.php/s/MGzoiiyMYEmtfRN){:target="_blank"} 
[*IM&#233;RA*]
* [Digital footprints, human mobility and cities](https://www.mmmycloud.com/index.php/s/rbBajGKPNMJK3mn){:target="_blank"} 
[*NECTAR CL8*]

<h3 class="page-title">2015</h3>

* [Toward a better understanding of cities using geolocated data](https://www.mmmycloud.com/index.php/s/55QAdHwXSroaE5J){:target="_blank"}
[*LISC*]
* [Functional Network of the City](https://www.mmmycloud.com/index.php/s/XjzSks4Lj8pCRBW){:target="_blank"} 
[*NetSci*]
<a href="https://hal.inrae.fr/hal-02890681" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Comparer et mod&#233;liser l'utilisation des sols](https://www.mmmycloud.com/index.php/s/SE9dzH67p4sz4W4){:target="_blank"} 
[*TheoQuant*]
<a href="https://hal.inrae.fr/hal-02890666" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Foraging in cities](https://www.mmmycloud.com/index.php/s/Z8NJXscjZ48sfHe){:target="_blank"}
[*Quanturb*]
* [Measuring the influence of cities using geolocated tweets](https://www.mmmycloud.com/index.php/s/z7c8EqLr8Lqbjkr){:target="_blank"}
[*NetMob*]
<a href="https://hal.inrae.fr/hal-02889642" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Spatial structure and efficiency of commuting in Senegalese cities](https://www.mmmycloud.com/index.php/s/dM7BtJnY5D8DZcp){:target="_blank"}
[*NetMob*]
<a href="https://hal.science/hal-03826984" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2014</h3>

* [Comparing and modeling land use organization in cities](https://www.mmmycloud.com/index.php/s/MH9zpzCgJjLCmXQ){:target="_blank"}
[*IFISC*]
* [Analysis of credit card data: how people move and spend their money.](https://www.mmmycloud.com/index.php/s/owa9zJ3gPy7oSpe){:target="_blank"} 
[*EUNOIA*]
* [Functional Network of the City](https://www.mmmycloud.com/index.php/s/o6NoggEepWSyS78){:target="_blank"}
[*CitiNet*]
<a href="https://hal.inrae.fr/hal-02890655" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Cross-checking different sources of mobility information](https://www.mmmycloud.com/index.php/s/Q2y7iYw32CDiBsM){:target="_blank"} 
[*ECCS*]
<a href="https://hal.inrae.fr/hal-02890644" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Toward a better understanding of cities using geolocated data](https://www.mmmycloud.com/index.php/s/ssWR53yNrRJDX4H){:target="_blank"}
[*EconoSocioF&#237;sica*]

<h3 class="page-title">2013</h3>

* [Universality Laws in Commuting Flows](https://www.mmmycloud.com/index.php/s/xkq5cC9pw34xJeB){:target="_blank"} 
[*ECCS*]
<a href="https://hal.inrae.fr/hal-02889638" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [A Universal Model of Commuting Networks](https://www.mmmycloud.com/index.php/s/ZDLp2LDGKnTXDHN){:target="_blank"}
[*Urbannet*]
<a href="https://hal.inrae.fr/hal-02890635" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [A Universal Model of Commuting Networks](https://www.mmmycloud.com/index.php/s/X76mSYgc3bi5kpX){:target="_blank"}
[*IFISC*]

<h3 class="page-title">2012</h3>

* [Initialize and calibrate a dynamic stochastic microsimulation model](https://www.mmmycloud.com/index.php/s/QWptpdjpXbdcHqG){:target="_blank"}
**[*PhD*]**
* [Estimation de param&#232;tres avec le calcul bay&#233;sien approch&#233; (ABC)](https://www.mmmycloud.com/index.php/s/72KjMCMQF4LaZBa){:target="_blank"} 
[*Mexico*]
* [Adaptive approximate Bayesian computation for complex models](https://www.mmmycloud.com/index.php/s/c9NbfXbtAgzRYLf){:target="_blank"} 
[*WCPS*]
<a href="https://hal.inrae.fr/hal-02597181" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Adaptive approximate Bayesian computation for complex models](https://www.mmmycloud.com/index.php/s/ctNkHfDmYoxkHqB){:target="_blank"}
[*ISBA*]
<a href="https://hal.inrae.fr/hal-02889634" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [G&#233;n&#233;ration et validation d'une population synth&#233;tique](https://www.mmmycloud.com/index.php/s/oxTLdGSkiNyjYxZ){:target="_blank"}
[*naXys*]

<h3 class="page-title">2011</h3>

* [A commuting generation model requiring only aggregated data](https://www.mmmycloud.com/index.php/s/BT9XAPTYr4zq52J){:target="_blank"}
[*ESSA*]
<a href="https://hal.inrae.fr/hal-02596121" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
* [Calibrating a complex social model](https://www.mmmycloud.com/index.php/s/sZtf4GMGKAmeAYm){:target="_blank"} 
[*ECCS*]
<a href="https://hal.inrae.fr/hal-02596122" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2010</h3>

* [From the Auvergne commuting network to every commuting network](https://www.mmmycloud.com/index.php/s/Pqc3nLddEXqcLAE){:target="_blank"} 
[*ECCS*]
<a href="https://hal.inrae.fr/hal-02594284" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-talk"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<h3 class="page-title">2009</h3>

* [Analyse de l'&#233;volution des communes d'Auvergne](https://www.mmmycloud.com/index.php/s/e3sWEZpnyKQfXcS){:target="_blank"}
**[*MSc*]**
