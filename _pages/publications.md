---
layout: page
title: Publications
permalink: /publications/
---

<h1 class="page-title">Publications</h1>

<div style="height:15px;">&nbsp;</div>

On this page, you'll find a list of my publications, along with links to 
associated materials such as PDFs, source files, source code, and visualization 
repositories when available. A BibTeX entry for citation is provided at the 
end of the page.

<div style="height:10px;">&nbsp;</div>

<!-- Peer-reviewed journal articles -->
<h3 class="page-title">Peer-reviewed journal articles</h3>

<div style="height:15px;">&nbsp;</div>

0. <a id="pvsspaper"></a> 
Lenormand M, F&#233;ret JB, Papuga G, Alleaume S & Luque S (2025) 
[Coupling in situ and remote sensing data to assess &#945;- and &#946;-diversity over biogeographic gradients.](https://arxiv.org/abs/2404.18485){:target="_blank"}
*Ecography* (in press).  
**Open science:**
[[PDF](https://arxiv.org/pdf/2404.18485.pdf){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2404.18485){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#pvsscode">repo</a>]
<!--
<a href="https://hal.science/hal-04109508" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
-->

0. <a id="bioregionpaper"></a> 
Denelle P, Leroy B & Lenormand M (2025) 
[Bioregionalization analyses with the bioregion R package.](https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.14496){:target="_blank"}
*Methods in Ecology and Evolution* 16, 496-506.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/za5snPnQKjHb8yS){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2404.15300){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#bioregionpackage">package</a>]
<a href="https://hal.inrae.fr/hal-04966616" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="intersectionalitypaper"></a> 
Vall&#233;e J & Lenormand M (2024) 
[Intersectionality approach of everyday geography.](https://journals.sagepub.com/doi/abs/10.1177/23998083231174025){:target="_blank"}
*Environment and Planning B: Urban Analytics and City Science* 51, 347-365.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/jmmgY5mFw3wks6D){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2106.15492){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#intersectionalitycode">repo</a>]
[<a href="https://www.maximelenormand.com/visualizations#intersectionalityviz">viz</a>]
<a href="https://hal.inrae.fr/hal-04109508" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="tdlmpaper"></a> 
Lenormand M (2023) 
[TDLM: An R package for a systematic comparison of trip distribution laws and models.](https://joss.theoj.org/papers/10.21105/joss.05434#){:target="_blank"}
*Journal of Open Source Software* 8, 5434.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/XRnrQ2fgCZXLYjJ){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#tdlmpackage">package</a>]
<a href="https://hal.inrae.fr/hal-04180303" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M & Samaniego H (2023)
[Uncovering the socioeconomic structure of spatial and social interactions in cities.](https://www.mdpi.com/2413-8851/7/1/15){:target="_blank"}
*Urban Science* 7, 15.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/fMJoEJLDMAzkcXH){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2105.02519){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-03964682 " 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="samplebiasdmspaper"></a> 
Dubos N, Pr&#233;au C, Lenormand M, Papuga G, Montsarrat S, Denelle P, Le Louarn M, Heremans S, Roel M, Roche P & Luque S (2022)
[Assessing the effect of sample bias correction in species distribution models.](https://www.sciencedirect.com/science/article/pii/S1470160X22009608){:target="_blank"}
*Ecological Indicators* 145, 109487.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/SaWjmCJ7s99ByGY){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2103.07107){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#samplebiasdmscode">repo</a>] 
<a href="https://hal.inrae.fr/hal-03833433" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Dubos N, Lenormand M, Castello L, Oberdorff T, Guisan A & Luque S (2022)
[Protection gaps in Amazon floodplains will increase with climate change: Insight from the world's largest scaled freshwater fish.](https://onlinelibrary.wiley.com/doi/10.1002/aqc.3877){:target="_blank"}
*Aquatic Conservation: Marine and Freshwater Ecosystems* 32, 1830-1841.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/GQqgoLDAxWdgfEx){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2202.05142){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-03758499" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Pr&#233;au C, Tournebize J, Lenormand M, Alleaume S, Gouy Boussada V & Luque S (2022)
[Habitat connectivity in agricultural landscapes improving multi-functionality of constructed wetlands as nature-based solutions.](https://www.sciencedirect.com/science/article/pii/S0925857422001860?via%3Dihub){:target="_blank"}
*Ecological Engineering* 182, 106725.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/66bi3dTyd2Dczoj){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2207.03826){:target="_blank"}] 
<a href="https://hal.inrae.fr/hal-03719277" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="mismatchconnectivitypaper"></a> 
Pr&#233;au C, Dubos N, Lenormand M, Denelle P, Le Louarn M, Alleaume S & Luque S (2022)
[Dispersal-based species pools as sources of connectivity area mismatches.](https://link.springer.com/article/10.1007%2Fs10980-021-01371-y){:target="_blank"}
*Landscape Ecology* 37, 729-743.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/is4Fa2eY6S2H2Mr){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2105.06702){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#mismatchconnectivitycode">repo</a>] 
<a href="https://hal.inrae.fr/hal-03599618" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lee-Cruz L, Lenormand M, Cappelle J, Caron A, De Nys H, Peeters M, Bourgarel M, Roger F & Tran A (2021)
[Mapping of Ebola virus spillover: suitability and seasonal variability at the landscape scale.](https://journals.plos.org/plosntds/article?id=10.1371/journal.pntd.0009683){:target="_blank"}
*PLOS Neglected Tropical Diseases* 15, e0009683.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/cKXpoSw672Lc7dG){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2108.10708){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-03325244" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Pella H & Capra H (2021)
[Animal daily mobility patterns analysis using resting event networks.](https://appliednetsci.springeropen.com/articles/10.1007/s41109-021-00353-y){:target="_blank"}
*Applied Network Science* 6, 7.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/DcA2teDG92w384e){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1912.04567){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-03128249" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Arias JM, San Miguel M & Ramasco JJ (2020)
[On the importance of trip destination for modeling individual human mobility patterns.](https://royalsocietypublishing.org/doi/10.1098/rsif.2020.0673){:target="_blank"}
*Journal of the Royal Society Interface* 17, 20200673.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/Y4cxmYk4SnTSg5J){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2004.01435){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-02966791" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Chiffoleau Y, Brit A-C, Monnier M, Akermann G, Lenormand M & Sauc&#232;de M (2020)
[Coexistence of supply chains in a city's food supply: a factor for resilience?](https://link.springer.com/article/10.1007/s41130-020-00120-0){:target="_blank"}
*Review of Agricultural, Food and Environmental Studies* 101, 391-414.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/2DX3xe89KaaHpnF){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-02935288" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="gismcdaowapaper"></a>
Billaud O, Soubeyrand M, Luque L & Lenormand M (2020)
[Comprehensive decision-strategy space exploration for efficient territorial planning strategies.](https://www.sciencedirect.com/science/article/pii/S0198971520302490){:target="_blank"}
*Computers, Environment and Urban Systems* 83, 101516.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/ZWFWXBrBDbp6w8s){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1911.11460){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#gismcdaowacode">repo</a>]
<a href="https://hal.inrae.fr/hal-02889617" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="mpppaper"></a>
Jahel C, Lenormand M, Seck I, Apolloni A, Toure I, Faye C, Sall B, Lo M, Squarzoni Diaw C, Lancelot R & Coste C (2020)
[Mapping livestock movements in Sahelian Africa.](https://www.nature.com/articles/s41598-020-65132-8){:target="_blank"}
*Scientific Reports* 10, 8339.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/DjJ9ppCZtnBakQm){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1910.10476){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#mppcode">repo</a>]
<a href="https://hal.science/hal-02613614" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Samaniego H, Chaves JC, da Fonseca Vieira V, da Silva MAHB & Evsukoff AG (2020)
[Entropy as a Measure of Attractiveness and Socioeconomic Complexity in Rio de Janeiro Metropolitan Area.](https://www.mdpi.com/1099-4300/22/3/368#){:target="_blank"}
*Entropy* 22, 368.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/Ym6aPRtxxyjfYJo){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/2003.10340){:target="_blank"}]
<a href="https://hal.science/hal-02516839" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Bassolas A, Gallotti R, Lamanna F, Lenormand M & Ramasco JJ (2020)
[Scaling in the recovery of urban transportation systems from massive events.](https://www.nature.com/articles/s41598-020-59576-1){:target="_blank"}
*Scientific Reports* 10, 2746.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/Hi3oNtEsCe5JC4t){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1906.07967){:target="_blank"}] 
<a href="https://hal.science/hal-02523615" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Mazzoli M, Molas A, Bassolas A, Lenormand M, Colet P & Ramasco JJ (2019)
[Field Theory for recurrent mobility.](https://www.nature.com/articles/s41467-019-11841-2){:target="_blank"}
*Nature Communications* 10, 3895.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/GMBxdH8CEF2jQYk){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1908.11814){:target="_blank"}]
<a href="https://hal.science/hal-02275851" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>  
**Highlights:** [Nature Physics](https://www.mmmycloud.com/index.php/s/amFPecx9qw6mjtB){:target="_blank"}

0. <a id="biogeomedpaper"></a>
Lenormand M, Papuga G, Argagnon O, Soubeyrand M, De Barros G, Alleaume S & Luque S (2019)
[Biogeographical network analysis of plant species distribution in the Mediterranean region.](https://onlinelibrary.wiley.com/doi/full/10.1002/ece3.4718){:target="_blank"}
*Ecology & Evolution* 9, 237-250.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/RmZAaFKbk6LzP9q){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1803.05275){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#biogeomedcode">repo</a>]
[<a href="https://www.maximelenormand.com/visualizations#biogeomedviz">viz</a>]
<a href="https://hal.inrae.fr/hal-02608629" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="ahiapaper"></a>
Lenormand M, Luque S, Langemeyer J, Tenerelli P, Zulian G, Aalders I, Chivulescu S, Clemente P, Dick J, van Dijk J, van Eupen M, Giuca R, Kopperoinen L, Lellei-Kov&#225;cs E, Leone M, Lieskovsk&#253; J, Schirpke U, Smith A, Tappeiner U & Woods H (2018)
[Multiscale socio-ecological networks in the age of information.](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0206672){:target="_blank"}
*PLOS ONE* 13, e0206672.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/4XqxfWR7yHdWkCw){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1805.00734){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#ahiacode">repo</a>]
[<a href="https://www.maximelenormand.com/visualizations#ahiaviz">viz</a>]
<a href="https://hal.science/hal-01912007" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lamanna F, Lenormand M, Salas-Olmedo MH, Romanillos G, Gon&#231;alves B & Ramasco JJ (2018)
[Immigrant community integration in world cities.](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0191612){:target="_blank"}
*PLOS ONE* 13, e0191612.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/gE6P76RHpo3Bqzd){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1611.01056){:target="_blank"}] 
<a href="https://hal.science/hal-01734992" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>  
**Highlights:** [National Geographic](https://www.nationalgeographic.com/culture/article/researchers-using-twitter-data-immigration-migration-graphic){:target="_blank"}  

0. Barbosa-Filho H, Barthelemy M, Ghoshal G, James C R, Lenormand M, Louail T, Menezes R, Ramasco JJ, Simini F & Tomasini M (2018)
[Human Mobility: Models and Applications.](https://www.sciencedirect.com/science/article/pii/S037015731830022X){:target="_blank"}
*Physics Reports* 734, 1-74.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/HEmrgsxrwWJWNGa){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1710.00004){:target="_blank"}]
<a href="https://shs.hal.science/halshs-01813382" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="owapaper"></a>
Lenormand M (2018)
[Generating OWA weights using truncated distributions.](http://onlinelibrary.wiley.com/doi/10.1002/int.21963/full){:target="_blank"}
*International Journal of Intelligent Systems* 33, 791-801.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/W96dBi2Kj9kqj83){:target="_blank"}]
[[arXiv](https://arxiv.org/abs/1709.04328){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#owacode">repo</a>]
[<a href="https://www.maximelenormand.com/visualizations#owaviz">viz</a>]
<a href="https://hal.science/hal-01716743" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Louail T, Lenormand M, Arias JM & Ramasco JJ (2017)
[Crowdsourcing the Robin Hood effect in cities.](https://appliednetsci.springeropen.com/articles/10.1007/s41109-017-0026-3){:target="_blank"}
*Applied Network Science* 2, 11.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/3ZsEZjKWtbxfF4i){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1604.08394){:target="_blank"}]
<a href="https://hal.science/hal-01535480" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>  
**Highlights:** [Le Monde](https://www.mmmycloud.com/index.php/s/ANLzPQzM46yLLkw){:target="_blank"}

0. Lenormand M & Ramasco JJ (2016)
[Towards a Better Understanding of Cities Using Mobility Data.](http://www.ingentaconnect.com/contentone/alex/benv/2016/00000042/00000003/art00005){:target="_blank"}
*Built Environment* 42, 356-364.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/f55QXPPARzGBKkx){:target="_blank"}]
<a href="https://hal.science/hal-01445512" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Bassolas A, Lenormand M, Tugores A, Gon&#231;alves B & Ramasco JJ (2016)
[Touristic site attractiveness seen through Twitter.](http://www.epjdatascience.com/content/5/1/12){:target="_blank"}
*EPJ Data Science* 5, 12.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/2ZgP9L7jpHHcqdS){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1601.07741){:target="_blank"}]
<a href="https://hal.science/hal-01294337" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="trippaper"></a>Lenormand M, Bassolas A & Ramasco JJ (2016)
[Systematic comparison of trip distribution laws and models.](http://www.sciencedirect.com/science/article/pii/S0966692315002422){:target="_blank"}
*Journal of Transport Geography* 51, 158-169.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/7XAH8zAmgW9NNm9){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1506.04889){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#tripcode">repo</a>]
<a href="https://hal.science/hal-01175812" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Picornell M, Cant&uacute;-Ros OG, Louail T, Herranz R, Barthelemy M, Frias-Martinez E, San Miguel M & Ramasco JJ (2015)
[Comparing and modeling land use organization in cities.](http://rsos.royalsocietypublishing.org/content/2/12/150449){:target="_blank"}
*Royal Society Open Science* 2, 150459.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/5qKcDNWZKF9qqXC){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1503.06152){:target="_blank"}]
<a href="https://hal.science/hal-01134157" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="humandiff"></a> 
Lenormand M, Gon&#231;alves B, Tugores A & Ramasco JJ (2015)
[Human diffusion and city influence.](http://rsif.royalsocietypublishing.org/content/12/109/20150473){:target="_blank"}
*Journal of the Royal Society Interface* 12, 20150473.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/5fB3QyLiFPZ99LN){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1501.07788){:target="_blank"}]
[<a href="https://www.maximelenormand.com/visualizations#humandiffviz">viz</a>]
<a href="https://hal.science/hal-01111981" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Louail T, Cant&uacute;-Ros OG, Picornell M, Herranz R, Arias JM, Barthelemy M, San Miguel M & Ramasco JJ (2015)
[Influence of sociodemographic characteristics on human mobility.](http://www.nature.com/srep/2015/150520/srep10075/full/srep10075.html){:target="_blank"}
*Scientific Reports* 5, 10075.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/ADrJGDLoDa7MHma){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1411.7895){:target="_blank"}]
<a href="https://hal.science/hal-01092615" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Picornell M, Ruiz T, Lenormand M, Ramasco JJ, Dubernet T & Frias-Martinez E (2015)
[Exploring the potential of phone call data to characterize the relationship between social network and travel behavior.](http://link.springer.com/article/10.1007%2Fs11116-015-9594-1){:target="_blank"}
*Transportation* 42, 647-668.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/sXrEiNt3MJFqrTw){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-04642434" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Louail T, Lenormand M, Picornell M, Cant&uacute;-Ros OG, Herranz R, Frias-Martinez E, Ramasco JJ & Barthelemy M (2015)
[Uncovering the spatial structure of mobility networks.](http://www.nature.com/ncomms/2015/150121/ncomms7007/full/ncomms7007.html){:target="_blank"}
*Nature Communications* 6, 6007.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/kNeJCpFsSxFQHm8){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1501.05269){:target="_blank"}]
<a href="https://hal.science/hal-01111731" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Campanelli B, Lenormand M, Fleurquin P, Ramasco JJ & Egu&#237;luz VM (2014)
[Movilidad y transporte: un viaje a traves del espacio, de la ciudad al mundo.](http://revistadefisica.es/index.php/ref/issue/view/132/showToc){:target="_blank"}
*Revista Espa&#241;ola de F&#237;sica* 28, 37-41.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/yAiDPidEKHiiFfk){:target="_blank"}]
<a href="https://hal.science/hal-04956966" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Tugores A, Colet P & Ramasco JJ (2014)
[Tweets on the road.](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0105407){:target="_blank"}
*PLOS ONE* 9, e105407.  
**Open science:**
[[arXiv](http://arxiv.org/abs/1405.5070){:target="_blank"}]
[[PDF](https://www.mmmycloud.com/index.php/s/N3g9BjZ2Fxp2cnJ){:target="_blank"}]
<a href="https://hal.science/hal-01086159" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Picornell M, Cant&uacute;-Ros OG, Tugores A, Louail T, Herranz R, Barthelemy M, Frias-Martinez E & Ramasco JJ (2014)
[Cross-checking different sources of mobility information.](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0105184){:target="_blank"} 
*PLOS ONE* 9, e105184.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/eSdmAFLyTEM7Mtg){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1404.0333){:target="_blank"}]
<a href="https://hal.science/hal-01086158" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0.  <a id="hotspot"></a> 
Louail T, Lenormand M, Cant&uacute;-Ros OG, Picornell M, Herranz R, Frias-Martinez E, Ramasco JJ & Barthelemy M (2014)
[From mobile phone data to the spatial structure of cities.](http://www.nature.com/srep/2014/140613/srep05276/full/srep05276.html){:target="_blank"}
*Scientific Reports* 4, 5276.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/SrmrJ8pgWpcDZWz){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1401.4540){:target="_blank"}]
<a href="https://hal.science/hal-01086156" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>  
**Highlights:** [MIT Tech Review](https://www.technologyreview.com/2014/01/27/174442/how-a-new-science-of-cities-is-emerging-from-mobile-phone-data-analysis/){:target="_blank"}

0. Lenormand M, Huet S & Gargiulo F (2014)
[Generating French virtual commuting network at municipality level.](https://www.jtlu.org/index.php/jtlu/article/view/360){:target="_blank"}
*Journal of Transport and Land Use* 7, 43-55.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/sZPR4BnFK9CFkkn){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1109.6759){:target="_blank"}]
<a href="https://hal.science/hal-00625229" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M & Deffuant G (2013)
[Generating a synthetic population of individuals in households: Sample-free vs sample-based methods.](http://jasss.soc.surrey.ac.uk/16/4/12.html){:target="_blank"}
*Journal of Artificial Societies and Social Simulation* 16, 12.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/yQQpww6tJ2QD8CA){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1208.6403){:target="_blank"}]
<a href="https://hal.science/hal-00725531" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. <a id="apmcpaper"></a>Lenormand M, Jabot F & Deffuant G (2013)
[Adaptive approximate Bayesian computation for complex models.](http://link.springer.com/article/10.1007%2Fs00180-013-0428-3){:target="_blank"}
*Computational Statistics* 28, 2777-2796.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/DxpTyok4xHLq3TY){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1111.1308){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#apmccode">repo</a>]
<a href="https://hal.science/hal-00638484" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Huet S, Gargiulo F & Deffuant G (2012)
[A universal model of commuting networks.](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0045985){:target="_blank"}
*PLOS ONE* 7, e45985.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/iYQAJmbwjgxNNiq){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1203.5184){:target="_blank"}]
<a href="https://hal.science/hal-00681772" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Lenormand M, Huet S & Deffuant G (2012)
[Deriving the number of jobs in proximity services from the number of inhabitants in French rural municipalities.](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0040001){:target="_blank"}
*PLOS ONE* 7, e40001.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/KL6wZtXx4zniQmY){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1109.6760){:target="_blank"}]
<a href="https://hal.science/hal-00625233" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

0. Gargiulo F, Lenormand M, Huet S & Baqueiro Espinosa O (2012)
[Commuting network model: Getting the essentials.](http://jasss.soc.surrey.ac.uk/15/2/6.html){:target="_blank"}
*Journal of Artificial Societies and Social Simulation* 15, 6.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/77wc7p9JFzgMcam){:target="_blank"}]
[[arXiv](http://arxiv.org/abs/1102.5647){:target="_blank"}]
<a href="https://hal.science/hal-02596993" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
{: reversed="reversed"}

<div style="height:30px;">&nbsp;</div>

<!-- Submitted papers -->
<h3 class="page-title">Submitted papers</h3>

<div style="height:15px;">&nbsp;</div>

* Souza Oliveira M, Lenormand M, Luque S, Zamora NA, Alleaume S, Aguilar Porras AC, 
Castillo MU, Chac&#243;n-Madrigal E, Delgado D, Hern&#225;ndez S&#225;nchez LG, Ngo Bieng M-A, 
Quesada RM, Solano GS, Z&#250;&#241;iga PM (2025) [Unlocking tropical forest complexity: How tree assemblages in
secondary forests boost biodiversity conservation.](https://arxiv.org/pdf/2503.02523.pdf){:target="_blank"}
*arXiv preprint* arXiv:2503.02523.  
**Open science:**
[[arXiv](https://arxiv.org/abs/2503.02523){:target="_blank"}]
<!--
<a href="https://hal.inrae.fr/" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
-->

* Soret M, Moulherat S, Lenormand M & Luque S (2024) [Implication of modelling choices on connectivity estimation: A comparative analysis.](https://arxiv.org/pdf/2407.09564.pdf){:target="_blank"}
*arXiv preprint* arXiv:2407.09564.  
**Open science:**
[[arXiv](https://arxiv.org/abs/2407.09564){:target="_blank"}]
<!--
<a href="https://hal.inrae.fr/" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>
-->

<div style="height:30px;">&nbsp;</div>

<!-- Conference papers -->
<h3 class="page-title">Conference papers</h3>

<div style="height:15px;">&nbsp;</div>

* <a id="msdpaper"></a> 
Lenormand M (2023) [Mapping mobile service usage diversity in cities.](https://www.mmmycloud.com/index.php/s/Jr793S6zZg8REPx){:target="_blank"}
*NetMob 2023*, Madrid, Spain.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/2dxfkGRCQgJ2kKG){:target="_blank"}]
[[arXiv extended version](https://arxiv.org/abs/2311.06269){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#msdcode">repo</a>]
<a href="https://hal.inrae.fr/hal-04231514" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

* <a id="mflpaper"></a> 
Lenormand M, Louail T, Barthelemy M & Ramasco JJ (2016) [Is spatial information in ICT data reliable?](https://hal.science/hal-01348458){:target="_blank"}
*Spatial Accuracy 2016*, Montpellier, France.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/nrQNxrZETSKQy8R){:target="_blank"}]
[[arXiv extended version](http://arxiv.org/abs/1609.03375){:target="_blank"}]
[<a href="https://www.maximelenormand.com/software#mflcode">repo</a>]
<a href="https://hal.science/hal-01368922" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

* Louf R, Carra G, Commenges H, Dembele J-M, Gallotti R, Lenormand M, Louail T & Barthelemy M (2015) [Spatial structure and efficiency of commuting in Senegalese cities.](https://www.mmmycloud.com/index.php/s/HR4yBNCnGJM38dj){:target="_blank"}
*NetMob 2015*, Boston, United States.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/rFLTTrQPLs7LYQP){:target="_blank"}]
<a href="https://hal.science/hal-03826984" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:30px;">&nbsp;</div>

<!-- Book chapters -->
<h3 class="page-title">Book chapters</h3>

<div style="height:15px;">&nbsp;</div>

* Huet S, Lenormand M, Deffuant G & Gargiulo F (2014)
[Parametrisation of individual working dynamics.](http://link.springer.com/chapter/10.1007/978-1-4614-6134-0_8){:target="_blank"}
*Chapter 8 in Empirical Agent-Based Modelling - Challenges and Solutions - Volume 1: The Characterisation and Parameterisation of Empirical Agent-Based Models*, 
Smajgl A & Barreteau O (Eds.), Springer New York, 133-169.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/oRA7CQaEXwKyfJ6){:target="_blank"}]
<a href="https://hal.inrae.fr/hal-02598836" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:30px;">&nbsp;</div>

<!-- Theses -->
<h3 class="page-title">Theses</h3>

<div style="height:15px;">&nbsp;</div>

* Lenormand M (2012) 
[Initialize and calibrate a dynamic stochastic microsimulation model: Application to the *SimVillages* model.](https://theses.fr/2012CLF22315){:target="_blank"}
*PhD thesis*, Blaise Pascal university, Clermont-Ferrand, France.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/jZoDmTjqx45nTfp){:target="_blank"}]
[[LaTeX](https://www.mmmycloud.com/index.php/s/Z23rfznptr3Ed45){:target="_blank"}]
<a href="https://theses.hal.science/tel-00764929" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

* Lenormand M (2009)
[Analyse de donn&#233;es relatives à l'&#233;volution des communes d'Auvergne pour la s&#233;lection de communes prototypiques.](https://www.mmmycloud.com/index.php/s/Fjy7DAkFTFTNryk){:target="_blank"}
*MSc thesis*, Blaise Pascal university, Clermont-Ferrand, France.  
**Open science:**
[[PDF](https://www.mmmycloud.com/index.php/s/Fjy7DAkFTFTNryk){:target="_blank"}]
[[LaTeX](https://www.mmmycloud.com/index.php/s/n5xNjZ7PQrjQ3Wc){:target="_blank"}]
<a href="https://dumas.ccsd.cnrs.fr/dumas-04963424" 
   target="_blank" 
   style="display: inline-flex; vertical-align: top;">
   <img src="{{ site.baseurl }}/assets/img/HAL.svg"
       class="hal-publi"
       alt="HAL" 
       style="width: 20px; margin-left: 1px;">
</a>

<div style="height:40px;">&nbsp;</div>

<center>
    <a href="https://www.mmmycloud.com/index.php/s/a4dpY92B93gza3i" target="_blank">
       <img src="{{ '/assets/img/BIBTEX.png' | relative_url }}" 
            alt="BIBTEX logo"
            border="0" 
            height="50">
    </a>
</center>
