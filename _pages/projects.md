---
layout: page
title: Projects
permalink: /projects/
---

<h1 class="page-title">Research projects</h1>

<div style="height:15px;">&nbsp;</div>

This page presents my ongoing and completed research projects, along with relevant details and collaborations.

<div style="height:10px;">&nbsp;</div>

<!-- On-going 

[**MOSIS (A network approach for quantifying the impact of change)**](https://){:target="_blank"} 
is a Young Researchers project funded by the French National Research Agency. 
The purpose of the NetCost project is to develop a conceptual framework 
which aims at quantifying, quantitatively and qualitatively, the cost of 
changes occurring in a complex system by measuring their impact at different 
scales, with a particular focus on how these changes will affect the individual 
elements forming the system. The proposed framework, relying on complex network 
science, will be applied on three different complex spatial systems selected 
for their importance in urban planning, ecology and spatial economy. The project
started on January 2018 and will run for four years. 
[[Role: Role: Investigator](https://){:target="_blank"}]

<div style="height:10px;">&nbsp;</div>

-->

[**NetCost (A network approach for quantifying the impact of change)**](https://netcost.gitlab.io/){:target="_blank"} 
was a Young Researchers project funded by the French National Research 
Agency. Its goal was to develop a conceptual framework for quantifying the cost 
of changes in complex systems by measuring their impact at different scales, 
with a focus on individual elements within the system. The framework, based on 
complex network science, was applied to three spatial systems relevant to urban 
planning, ecology, and spatial economy. The project ran from January 2018 to 
March 2023. 
[[Role: Scientific Coordinator](https://netcost.gitlab.io/){:target="_blank"}]  

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://netcost.gitlab.io/" target="_blank">
        <img src="{{ '/assets/projects/NetCost.png' | relative_url }}" 
             alt="NetCost project logo"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**BONDS (Balancing BiOdiversity CoNservation with Development in Amazonian WetlandS)**](http://www.bonds-amazonia.org/){:target="_blank"} 
is a Belmont Forum and BiodivERsA project funded through the International joint
call on "Scenarios of Biodiversity and Ecosystem Services". The purpose of BONDS
was to combine conservation and development stakes in scenarios of biodiversity 
and ecosystem services for the extensive floodplains of the world's largest 
river system: the Amazon. The project ran from March 2019 to March 2022. 
[[Role: Investigator](http://www.bonds-amazonia.org/){:target="_blank"}]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="http://www.bonds-amazonia.org/" target="_blank">
        <img src="{{ '/assets/projects/BONDS.png' | relative_url }}"
             alt="BONDS project logo"
             border="0" 
             height="250">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**IMAGINE (Integrative Management of Green Infrastructures Multifunctionality, Ecosystem Integrity and Ecosystem Services: From assessment to regulation in socioecological systems)**](https://www.biodiversa.eu/2022/10/31/imagine/){:target="_blank"} 
Using a multidisciplinary approach across six case study territories spanning a 
European north-south gradient from the boreal zone to the Mediterranean, the 
IMAGINE project aimed at quantifying the multiple functions, ecosystem services,
and benefits provided by Green Infrastructures (GI) in different contexts from 
rural to urban. The project ran from February 2017 to January 2020. 
[[Role: Investigator](https://www.biodiversa.eu/2022/10/31/imagine/){:target="_blank"}]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://www.biodiversa.eu/2022/10/31/imagine/" target="_blank">
        <img src="{{ '/assets/projects/IMAGINE.jpg' | relative_url }}" 
             alt="IMAGINE project logo"
             border="0" 
             height="150">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**Cultural Ecosystem Services: An Empirical approach across European landscapes and beyond**](https://alterneteurope.eu/){:target="_blank"} 
is an AHIA research project funded under the ALTER-Net High Impact Actions. 
The project ran from February 2017 to February 2018. 
[[Role: Scientific Coordinator](https://alterneteurope.eu/){:target="_blank"}] 

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://alterneteurope.eu/" target="_blank">
        <img src="{{ '/assets/projects/AlterNet.png' | relative_url }}" 
             alt="ALTER-Net logo"
             border="0" 
             height="170">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**INSIGHT (Innovative Policy Modelling and Governance Tools for Sustainable Post-Crisis Urban Development)**](http://www.insight-fp7.eu/){:target="_blank"} 
is a research project funded under the ICT Theme of the European Union's Seventh 
Framework Programme. INSIGHT aimed to investigate how ICT, with particular focus 
on data science and complexity theory, can help European cities formulate and 
evaluate policies to stimulate a balanced economic recovery and sustainable urban 
development. The project ran from October 2013 to October 2016.
[[Role: Investigator](http://www.insight-fp7.eu/){:target="_blank"}] 

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="http://www.insight-fp7.eu/" target="_blank">
        <img src="{{ '/assets/projects/INSIGHT.jpg' | relative_url }}" 
             alt="Insight project logo"
             border="0" 
             height="100">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**EUNOIA (Evolutive User-centric Networks for Intraurban Accessibility)**](https://www.nommon.es/research-projects/eunoia/){:target="_blank"} 
is a research project funded under the European Union's Seventh Framework 
Programme ICT Programme. The goal of EUNOIA was to take advantage of the 
opportunities brought by smart city technologies and the most recent advances in
complex systems science to develop new urban models and ICT tools empowering 
city governments and their citizens to design better mobility policies. The 
project ran from October 2012 to October 2014. 
[[Role: Investigator](https://www.nommon.es/research-projects/eunoia/){:target="_blank"}]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://www.nommon.es/research-projects/eunoia/" target="_blank">
        <img src="{{ '/assets/projects/EUNOIA.png' | relative_url }}" 
             alt="Eunoia project logo"
             border="0" 
             height="100">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

[**PRIMA (Prototypical Policy Impacts on Multifunctional Activities in rural municipalities)**](https://){:target="_blank"} 
aimed to develop a method for scaling down the analysis of policy impacts on 
multifunctional land uses and on economic activities. This method relied on 
micro-simulation and multi-agent models, designed and validated at the 
municipality level, using input from stakeholders. The models addressed the 
structural evolution of populations (appearance, disappearance, and change of 
agents) depending on the local conditions for applying the structural policies 
on a set of municipality case studies.
[[Role: Investigator](https://){:target="_blank"}] 

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://" target="_blank">
        <img src="{{ '/assets/projects/PRIMA.png' | relative_url }}"
             alt="PRIMA project logo"
             border="0" 
             height="70">
    </a>
</center>
