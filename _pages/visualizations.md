---
layout: page
title: Visualizations
permalink: /visualizations/
---

<h1 class="page-title">Data visualizations</h1>

<div style="height:15px;">&nbsp;</div>

This page gathers some interactive web applications that I have designed and 
developed (with my colleagues and students). It also includes some older 
data visualizations.

<div style="height:10px;">&nbsp;</div>

<!-- Interactive web applications -->
<a id="iwa"></a>
<h3 class="page-title">Interactive web applications</h3>

<div style="height:10px;">&nbsp;</div>

<a id="biblioviz"></a>
[**Biblio-TETIS**](https://biblio-tetis.sk8.inrae.fr){:target="_blank"} 
is an interactive web application providing a bibliographic analysis of 
publications (journal articles and conference papers) from the UMR TETIS 
between 2016 and 2024.  
**Open science:**
[<a href="https://www.maximelenormand.com/software#bibliocode">repo</a>]

<center>
    <a href="https://biblio-tetis.sk8.inrae.fr" target="_blank">
        <img src="{{ '/assets/vizs/Biblio-TETIS.png' | relative_url }}" 
             alt="Screenshot of the Biblio-TETIS Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<div style="height:10px;">&nbsp;</div>

<a id="intersectionalityviz"></a>
[**Intersectionality**](https://intersectionality.sk8.inrae.fr/){:target="_blank"} 
is an interactive web application designed to provide an easy-to-use interface 
to visualize the gender-, age- and educational-based mismatch in hourly 
population profiles at district level in France.  
**Open science:**
[<a href="https://www.maximelenormand.com/software#intersectionalitycode">repo</a>]
[<a href="https://www.maximelenormand.com/publications#intersectionalitypaper">paper</a>]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://intersectionality.sk8.inrae.fr/" target="_blank">
        <img src="{{ '/assets/vizs/Intersectionality.png' | relative_url }}" 
             alt="Screenshot of the Intersectionality Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<a id="biogeofrviz"></a>
[**BiogeoFr**](https://biogeofr.sk8.inrae.fr){:target="_blank"} 
is an interactive web application designed to provide an easy-to-use interface 
for visualizing the major phytogeographical regions of mainland France. 
<!--
**Open science:**
-->

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://biogeofr.sk8.inrae.fr" target="_blank">
        <img src="{{ '/assets/vizs/BiogeoFr.png' | relative_url }}" 
             alt="Screenshot of the BiogeoFr Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<a id="biogeomedviz"></a>
[**BiogeoMed**](https://biogeomed.sk8.inrae.fr){:target="_blank"} 
is an interactive web application designed to provide an easy-to-use interface 
for visualizing multiscale biogeographical structures of plant species 
distribution in the south of France.  
**Open science:**
[<a href="https://www.maximelenormand.com/software#biogeomedcode">repo</a>]
[<a href="https://www.maximelenormand.com/publications#biogeomedpaper">paper</a>]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://biogeomed.sk8.inrae.fr" target="_blank">
        <img src="{{ '/assets/vizs/BiogeoMed.png' | relative_url }}" 
             alt="Screenshot of the BiogeoMed Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<a id="ahiaviz"></a>
[**AHIA**](https://ahia.sk8.inrae.fr){:target="_blank"} 
is an interactive web application designed to provide an easy-to-use interface to 
visualize socio-ecological interactions at different scales in 16 case studies 
across Europe.  
**Open science:**
[<a href="https://www.maximelenormand.com/software#ahiacode">repo</a>]
[<a href="https://www.maximelenormand.com/publications#ahiapaper">paper</a>]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://ahia.sk8.inrae.fr" target="_blank">
        <img src="{{ '/assets/vizs/AHIA.png' | relative_url }}" 
             alt="Screenshot of the AHIA Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<a id="owaviz"></a>
[**OWA**](https://owa.sk8.inrae.fr){:target="_blank"} 
is an interactive web application designed to provide an easy way to generate 
OWA weights according to a certain level of risk and trade-off using 
truncated distributions.  
**Open science:**
[<a href="https://www.maximelenormand.com/software#owacode">repo</a>]
[<a href="https://www.maximelenormand.com/publications#owapaper">paper</a>]

<div style="height:10px;">&nbsp;</div>

<center>
    <a href="https://owa.sk8.inrae.fr" target="_blank">
        <img src="{{ '/assets/vizs/OWA.png' | relative_url }}" 
             alt="Screenshot of the OWA Shiny interactive web application"
             border="0" 
             height="300">
    </a>
</center>

<div style="height:30px;">&nbsp;</div>

<!-- Old stuff -->
 <a id="oldvizs"></a> 
<h3 class="page-title">Old stuff</h3>

<div style="height:10px;">&nbsp;</div>

 <a id="humandiffviz"></a> 
**Human diffusion**

<video controls width="700px">
  <source src="{{ '/assets/vizs/Diffusion_Barcelona.mp4' | relative_url }}"
          type="video/mp4" />
</video>

<div style="height:30px;">&nbsp;</div>

**Hotspots**

<video controls width="400px">
  <source src="{{ '/assets/vizs/Hotspot_Barcelona.mp4' | relative_url }}" 
          type="video/mp4" />
</video>

<div style="height:30px;">&nbsp;</div>

 <a id="kmlviz"></a> 
**Credit card data**

<video controls width="700px">
  <source src="{{ '/assets/vizs/Trajectories_Madrid.mp4' | relative_url }}" 
          type="video/mp4" />
</video>

<div style="height: 350px;"></div>

