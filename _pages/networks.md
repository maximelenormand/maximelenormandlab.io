---
layout: page
title: networks
permalink: /networks/
---

<h1 class="page-title">Scientific networks</h1>

<div style="height:15px;">&nbsp;</div>

This page presents my scientific networks, including supervisions, 
collaborations, visits, teaching activities, and review contributions.

<div style="height:10px;">&nbsp;</div>

<!-- People -->
<h3 class="page-title"> People </h3>

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">Undergraduate and MSc students</span> </h5>
* [Yoann Debain](https://www.researchgate.net/profile/Yoann_Debain){:target="_blank"} (2022)
* [Larisa Lee-Cruz](https://www.researchgate.net/profile/Larisa_Lee-Cruz){:target="_blank"} (2019)
* [Mary Varoux](https://www.researchgate.net/profile/Mary_Varoux){:target="_blank"} (2019)
* [Maxence Soubeyrand](https://scholar.google.com/citations?user=tAPNCJQAAAAJ&hl=en){:target="_blank"} (2018)
* [Milo Monnier](https://www.researchgate.net/profile/Milo_Monnier){:target="_blank"} (2018) 
* [S&#233;bastien Poilroux](https://fr.linkedin.com/in/sebastien-poilroux-a8b85388){:target="_blank"} (2017)  
* [Javier Moreno Gordo](https://es.linkedin.com/in/javier-moreno-gordo-a1863575){:target="_blank"} (2014)  
* [Mamourou Felemou](https://fr.linkedin.com/in/mamourou-felemou-1087ab4a){:target="_blank"} (2011)

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">PhD students</span> </h5>
* [Marie Soret](http://terroiko.fr/cvmarie.php?lng=en_GB){:target="_blank"} (2020-2024) 
* [Milo Monnier](https://www.researchgate.net/profile/Milo_Monnier){:target="_blank"} (2018-2020) 
* [Aleix Bassolas](https://scholar.google.com/citations?user=tXaOGyUAAAAJ&hl=en){:target="_blank"} (2014-2019)  

<h5 class="page-title"> <span style="color:gray;font-weight: bold;">Postdocs</span> </h5>
* [Rémi Perrier](https://www.researchgate.net/profile/Remi-Perrier){:target="_blank"} (2024-2025)
* [Marina Gomtsyan](https://scholar.google.fr/citations?hl=fr&user=dkvZQ-UAAAAJ&view_op=list_workse){:target="_blank"} (2024-2025)
* [Andrea Russo](https://scholar.google.com/citations?hl=en&user=uuWmYqwAAAAJ&view_op=list_works){:target="_blank"} (2023-2024)
* [Stanislas Rigal](https://www.researchgate.net/profile/Stanislas-Rigal-2){:target="_blank"} (2023-2025)
* [Larisa Lee-Cruz](https://www.researchgate.net/profile/Larisa_Lee-Cruz){:target="_blank"} (2022)
* [Nicolas Dubos](https://scholar.google.fr/citations?user=R7md47gAAAAJ&hl=fr){:target="_blank"} (2020-2021)
* [Larisa Lee-Cruz](https://www.researchgate.net/profile/Larisa_Lee-Cruz){:target="_blank"} (2020-2021)
* [Pierre Denelle](https://scholar.google.fr/citations?user=x66knTsAAAAJ&hl=en){:target="_blank"} (2019-2020) 
* [Marine Le Louarn](https://scholar.google.fr/citations?user=uEN2EUEAAAAJ&hl=en){:target="_blank"} (2018-2019)  

<h6 class="page-title"> <span style="color:gray;font-weight: bold;">Visiting scholars</span> </h6>
* [Horacio Samaniego](https://scholar.google.com/citations?user=VeJEo1AAAAAJ&hl=en){:target="_blank"} - Austral University of Chile (September 2018)
* [Jos&#233; Javier Ramasco](https://scholar.google.com/citations?user=aEdK_vsAAAAJ&hl=en){:target="_blank"} - University of the Balearic Islands (June 2018)
* [Vahid Rahdari](https://www.researchgate.net/profile/Vahid_Rahdari){:target="_blank"} - University of Isfahan (October 2016 - March 2017)

<!-- Coauthorship -->
<a id="coauthorship"></a>
<h6 class="page-title"> 
  <span style="color:gray;font-weight: bold;">
    Coauthorship network (Google Scholar - 2025-01-05) [<a href="https://www.maximelenormand.com/software#coauthorshipcode">repo</a>]
  </span>
</h6>

<div style="height:5px;">&nbsp;</div>

<p style="margin-left:-150px;margin-bottom: 0px;">
    <iframe width="115%" 
            height="450px" 
            src="{{ '/assets/vizs/Coauthorship.html' | relative_url }}" 
            frameborder="0"></iframe>
</p>  

<!-- Research stays -->
<h3 class="page-title"> Research stays </h3>

* [University of the Balearic Islands](http://www.uib.es/es/){:target="_blank"} (October-November 2020)
* [Austral University of Chile](http://international.uach.cl/){:target="_blank"} (March 2020)
* [University of the Balearic Islands](http://www.uib.es/es/){:target="_blank"} (April-May 2019)
* [Federal University of Rio de Janeiro](https://ufrj.br/){:target="_blank"} (October 2018)
* [Austral University of Chile](http://international.uach.cl/){:target="_blank"} (March 2018)
* [University of the Balearic Islands](http://www.uib.es/es/){:target="_blank"} (September 2017)

<div style="height:20px;">&nbsp;</div>

<!-- Organization of scientific events -->
<h3 class="page-title"> Organization of scientific events</h3>

* Annual workshop of the EIGHTIES research group - Saint-Pierre-d'Ol&#233;ron, France (September 23-27, 2019)
* Spatio-temporal trajectory analysis - Satellite workshop at the SAGEO conference, Rouen, France (November 6-9, 2017)
* UrbanNet2015 - Satellite workshop at the NetSci2015 conference, Zaragoza, Spain (June 1-5, 2015)

<div style="height:20px;">&nbsp;</div>

<!-- Teaching -->
<h3 class="page-title"> Teaching</h3>

* Advanced spatial analysis - MSc in Geomatics - AgroParisTech Montpellier (since 2018)
* Network analysis - Mast&#232;re SILAT - AgroParisTech Montpellier (since 2017)
* Probability & statistics - BSc in Computer Science - Blaise Pascal University (2011-2013)
* Informatic tools - MSc in Sport Science - Blaise Pascal University (2011-2013)
* Data analysis - MSc in Computer Science  - Blaise Pascal University (2011-2013)

<div style="height:20px;">&nbsp;</div>

<!-- Committee involvement -->
<h3 class="page-title"> Committee involvement </h3>
* PhD steering committee - [Mohamadou Salifou](https://scholar.google.com/citations?hl=en&user=R-fJpk4AAAAJ&view_op=list_works&sortby=pubdate){:target="_blank"} - [LISST](https://lisst.univ-tlse2.fr/){:target="_blank"} (2024 - 2025) 
* PhD steering committee - [Billy Pottier](https://www.researchgate.net/profile/Billy_Pottier){:target="_blank"} - [LGEI](http://lgei.mines-ales.fr/){:target="_blank"} (2019 - 2022) 
* PhD steering committee - [Diego Bengochea](https://www.researchgate.net/profile/Diego-Bengochea){:target="_blank"} - [CTMB](https://sete-moulis-cnrs.fr/fr/recherches/ctmb/){:target="_blank"} (2020 - 2022)
* PhD steering committee - [Léo Pichon](https://www.researchgate.net/profile/Leo_Pichon){:target="_blank"} - [ITAP Research Unit](https://itap.montpellier.hub.inrae.fr/){:target="_blank"} (2018) 
* PhD thesis external reviewer - [Jorge P Rodr&#237;guez](https://scholar.google.co.uk/citations?user=GqGSZEcAAAAJ&hl=en){:target="_blank"} - [IFISC](https://ifisc.uib-csic.es/){:target="_blank"} (2018)  
* PostDoc steering committee - [Dominique Lamonica](https://www.researchgate.net/profile/Dominique_Lamonica){:target="_blank"} - [Dynam Research Unit](https://ecoflows.inrae.fr/){:target="_blank"} (2016)  

<div style="height:20px;">&nbsp;</div>

<!-- Reviewer -->
<h3 class="page-title"> Reviewer for</h3>

* [Applied Mathematics and Computation](https://www.journals.elsevier.com/applied-mathematics-and-computation){:target="_blank"} **#1**
* [Autonomous Agents and Multi-Agent Systems](https://www.springer.com/journal/10458){:target="_blank"} **#1**
* [Chaos](https://aip.scitation.org/journal/cha){:target="_blank"} **#1**
* [Cities](https://www.sciencedirect.com/journal/cities){:target="_blank"} **#1**
* [Complex Networks conference](https://complexnetworks.org/) **#8**
* [Computational Social Networks](https://www.springer.com/mathematics/applications/journal/40649){:target="_blank"} **#1**
* [Computer Communications](https://www.journals.elsevier.com/computer-communications/){:target="_blank"}  **#1**
* [Computers, Environment and Urban Systems](https://www.sciencedirect.com/journal/computers-environment-and-urban-systems){:target="_blank"} **#1**
* [Data Mining and Knowledge Discovery](https://link.springer.com/journal/10618){:target="_blank"}  **#1**
* [Ecological Informatics](http://www.journals.elsevier.com/ecological-informatics){:target="_blank"}  **#1**
* [Ecological Modelling](https://www.journals.elsevier.com/ecological-modelling){:target="_blank"}  **#1**
* [Environment and Planning B: Urban Analytics and City Science](https://journals.sagepub.com/home/epb){:target="_blank"}  **#2**
* [EPJ Data Science](https://epjdatascience.springeropen.com/){:target="_blank"}  **#5**
* [French Regional Conference on Complex Systems](https://iutdijon.u-bourgogne.fr/ccs-france/){:target="_blank"}  **#2**
* [GeoJournal](https://link.springer.com/journal/10708){:target="_blank"}  **#1**
* [Historical Methods: A Journal of Quantitative and Interdisciplinary History](http://www.tandfonline.com/toc/vhim20/current){:target="_blank"} **#1**
* [International Journal of Data Science and Analytics](https://www.springer.com/journal/41060){:target="_blank"} **#1**
* [Journal of Artificial Societies and Social Simulation](http://jasss.soc.surrey.ac.uk/){:target="_blank"}  **#4**
* [Journal of Open Source Software](https://joss.theoj.org/){:target="_blank"}  **#1**
* [Journal of the Royal Society Interface](http://rsif.royalsocietypublishing.org/){:target="_blank"}  **#11**
* [Journal of Transport Geography](https://www.journals.elsevier.com/journal-of-transport-geography){:target="_blank"} **#3** 
* [Landscape Ecology](https://www.springer.com/journal/10980?gclid=EAIaIQobChMImLSQgabY7gIVPBoGAB1oeQBSEAAYASAAEgI_NPD_BwE){:target="_blank"} **#1** 
* [Landscape and Urban Planning](https://www.journals.elsevier.com/landscape-and-urban-planning/){:target="_blank"} **#1** 
* [Nature Communications](http://nature.com/ncomms/){:target="_blank"} **#6** 
* [One Ecosystem](https://oneecosystem.pensoft.net/){:target="_blank"}  **#1**
* [Pervasive and Mobile Computing](https://www.journals.elsevier.com/pervasive-and-mobile-computing){:target="_blank"} **#1** 
* [Physica A](https://www.journals.elsevier.com/physica-a-statistical-mechanics-and-its-applications){:target="_blank"}  **#3**
* [PLOS Computational Biology](https://journals.plos.org/ploscompbiol/){:target="_blank"}  **#1**
* [PLOS ONE](http://journals.plos.org/plosone/){:target="_blank"}  **#14**
* [Royal Society Open Science](http://rsos.royalsocietypublishing.org){:target="_blank"} **#3**
* [Scientific Data](https://www.nature.com/sdata/){:target="_blank"}  **#2**
* [Scientific Reports](http://nature.com/srep/index.html){:target="_blank"}  **#8**
* [Sustainable Cities and Society](https://www.journals.elsevier.com/sustainable-cities-and-society){:target="_blank"}  **#3**
* [Transaction in GIS](http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-9671){:target="_blank"}   **#1**
* [Transport](http://www.tandfonline.com/toc/tran20/current){:target="_blank"}  **#1**
* [Urban Planning](http://www.cogitatiopress.com/urbanplanning){:target="_blank"}  **#1**

<div style="height:20px;">&nbsp;</div>

<!-- Academic Editor -->
<h3 class="page-title"> Academic editor for</h3>

* [PLOS ONE](http://journals.plos.org/plosone/){:target="_blank"}  **#7**
